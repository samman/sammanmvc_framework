<!-- 
    Super class for every other controllers
    Base controller
    Loads modals and views
 -->

<?php

    class Controller {

        // load model
        public function model($model){

            // require modal file
            require_once '../app/models/' . $model . '.php';
            
            // instantiate the modal
            return new $model();
        }

        // load view
        public function view($view, $data=[]){
            $path = "../app/views/" . $view . ".php";

            // check for the view file
            if(file_exists($path)){

                require_once($path);

            }else{

                // view doesn't exist
                die("View does not exist");

            }

        }

    }
 
 ?>
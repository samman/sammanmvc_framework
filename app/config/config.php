<?php

    //DB Params
    define('DB_HOST', "_YOUR_HOST_");
    define('DB_USER', "_YOUR_USER_");
    define('DB_PASSWORD', "_YOUR_PASSWORD_");
    define('DB_NAME', "_YOUR_DB_NAME_");

    // App Root
    define('APPROOT', dirname(dirname(__FILE__)));

    // Url Root
    define('URLROOT', "_YOUR_APP_URL_ROOT_");

    // Site name
    define('SITENAME', '_YOUR_SITE_NAME_');

?> 
<?php

    class Pages extends Controller{
        public function __construct(){
            // Initialize the model here
        }

        public function index(){
            $this->view("pages/index");
        }

        public function about(){
            $this->view("pages/about");
        }
    }

?>